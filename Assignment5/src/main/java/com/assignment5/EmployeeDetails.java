package com.assignment5;


import java.util.HashMap;


import org.springframework.stereotype.Service;

@Service
public class EmployeeDetails {
	
	HashMap<String, Employee> map = new HashMap<String, Employee>();

	
	public EmployeeDetails() {
		
		
		map=init();
	}
	
	private static HashMap<String, Employee> init() {
	
		Employee s1 = new Employee(
				"1|Dhoni|dhoni@csk.com|chennai");
	
		Employee s2 = new Employee(
				"2|Kohli|kohli@rcb.com|Bangalore");
		
		Employee s3 = new Employee(
				"3|Gambhir|gambhir@dd.com|Delhi");
		
		Employee s4 = new Employee(
				"4|Rohit|rohit@Mi.com|Mumbai");
		
		Employee s5 = new Employee(
				"5|Karthik|karthik@kkr.com|Kolkatta");

		
		HashMap<String, Employee> map = new HashMap<String, Employee>();

		
		map.put(s1.getEmpid(), s1);
		map.put(s2.getEmpid(), s2);
		map.put(s3.getEmpid(), s3);
		map.put(s4.getEmpid(), s4);
		map.put(s5.getEmpid(), s5);

	
		return map;
	}
	
	
	
	
	
	
	public HashMap<String, Employee> getallemployees()
	{
		
		return map;
	}


	public Employee getemployee(String empid)
	
	{
	
		return map.get(empid);
	}


	public void addEmployee(Employee emp) {
		
		map.put(emp.getEmpid(), emp);
	}


	public void updateEmployee(String id, Employee emp) {
		map.put(id, emp);
	}






	public void deleteEmployee(String id) {
		
	        	
	        	map.remove(id);	
	        
	}
	
	
	
	
	

}

